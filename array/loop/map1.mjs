//Map always has return value
//[1,2,3]=>[3,6,9]
let nums = [1, 2, 3];
let num1 = nums.map((value, index) => {
    return value * 3;
});
console.log(num1);

//Add
let number = [1, 2, 3]
let num2 = number.map((value, index) => {
    return value + 10
})
console.log(num2);

//Uppercase
let input = ["my", "name", "is"]
let input1 = input.map((value, index) => {
    return value.toUpperCase()
})
console.log(input1);


// ['MY', 'NAME', 'IS'] => ['MYN', 'NAMEN', 'ISN']
let name = ["my", "name", "is"]
let name1 = name.map((value, index) => {
    return `${value.toUpperCase()}N`
})
console.log(name1);

//output as ['1111', '2111', '3111']
let example = [1, 2, 3]
let example1 = example.map((value, index) => {
    return `${value}111`
})
console.log(example1);


//output as ['1000a', '2111a', '3222a']
let output = [1, 2, 3]
let output1 = output.map((value, index) => {
    return `${value}${index}${index}${index}a`
})
console.log(output1);

let element = [1, 3, 4, 5]
let element1 = element.map((value, index) => {
    if (value % 2 === 0) {
        return value * 0
    } else {
        return value * 100
    }
})
console.log(element1);


