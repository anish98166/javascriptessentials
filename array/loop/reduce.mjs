let arr = [1, 2, 3, 4]; //10

let output = arr.reduce((previous, current) => {
  return previous + current;
}, 0); //initialValue

console.log(output);
