let arr = ["anish", 26, false];
console.log(arr.length);

// make a arrow function
// pass array of names
// the fun must return "arrays length is greater than 3"  if passes array length is greater than 3
//else the fun must return "array length is less than 3"
let arr1 = (names) => {
  if (names.length > 3) {
    return "array length is greater than 3";
  } else {
    return "array length is less than 3";
  }
};

let great = arr1(["anish", "nitan", 2, 4]);
console.log(great);
