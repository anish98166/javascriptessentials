// Array is used to store multiple value of same or different data types
// Array has indexing, indexing starts from 0
// Indexing is used to get or change particular elements of array

let name = ["anish", "roshan", "nitan", 29, false];
console.log(name);
console.log(name[2]);
console.log(name[4]);

name[2] = "manish";
console.log(name);
