let arr = ["anish", 26, false];

let arr1 = arr.includes("ram");
console.log(arr1); //false

let arr2 = arr.includes("ram");
console.log(arr2); //false

let arr3 = arr.includes("anish");
console.log(arr3); //true

//make a arrow function
// pass array of name
// the function must return "they can enter house" if the array contain "nitan"
//  else return "he can not enter house"

let house = (name) => {
  let names = name.includes("nitan");
  if (names) {
    return "he can enter house";
  } else {
    return "he cannot enter house";
  }
};

let enter = house(["nitan", "anish", "roshan"]);
console.log(enter);
