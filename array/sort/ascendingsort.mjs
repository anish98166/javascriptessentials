let arr = ["a", "c", "d", "b"];

let arr1 = arr.sort(); //[ 'a', 'b', 'c', 'd' ]
console.log(arr1);

let arr2 = [9, 10];
let arr3 = arr2.sort(); //[ 10, 9 ]-Interview question
console.log(arr3);

//Sorting starts from capital letters
