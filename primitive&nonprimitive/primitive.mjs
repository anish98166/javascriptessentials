//Primitive  = number, string, boolean, undefined, null
//Memory allocation
//if let is used memory allocation for that

let a = 1;
let b = a;
let c = 1;
a = 10;

//===
//in case of primitive === see value
console.log(a === b); //true
console.log(a === c); //true

console.log(a); //10
console.log(b); //1
console.log(c); //1

//Non Primitive = array, object, date ,error
//Memory allocation
//Before allocating memory on non primitive it checks whether the variable is copy of another variable, if the variable is copy of another then it will share memory

let arr = [1, 2, 3];
let arr1 = arr;
let arr2 = [1, 2, 3];

//===
//in case of non primitive === see memory address/allocation
console.log(arr === ar1); //true
console.log(arr === arr2); //false

arr.push(5);
console.log(arr); //[1,2,3,5]
console.log(arr1); //[1,2,3,5]
console.log(arr2); //[1,2,3]
