//In Object destructuring order/index does not matter/work.
//name must match

let { name, age, isMarried } = { name: "anish", age: 30, isMarried: false };
console.log(name);
console.log(age);
console.log(isMarried);
