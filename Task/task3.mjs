let product = [
  { name: "laptop", price: 100000 },
  { name: "mobile", price: 50000 },
  { name: "tv", price: 70000 },
];

let product1 = product.map((value, index) => {
  return value.name;
});
let product2 = product.map((value, index) => {
  return value.price;
});

let product3 = product.filter((value, index) => {
  if (value.price > 60000) {
    return true;
  }
});
let product5 = product3.map((value, index) => {
  return value.name;
});

let product4 = product.map((value, index) => {
  return { [value.name]: value.price };
});

console.log(product1); //[ 'laptop', 'mobile', 'tv' ]
console.log(product2); //[ 100000, 50000, 70000 ]
console.log(product3); //[ { name: 'laptop', price: 100000 }, { name: 'tv', price: 70000 } ]
console.log(product4); //[ { laptop: 100000 }, { mobile: 50000 }, { tv: 70000 } ]
console.log(product5); //[ 'laptop', 'tv' ]
