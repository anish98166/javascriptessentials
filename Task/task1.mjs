// make a arrow function name is18 pass a value ,the function must return true if the given age is 18 otherwise return false

let is18 = (age) => {
  if (age === 18) {
    return true;
  } else {
    return false;
  }
};

let year = is18(20);
console.log(year);

//make a arrow function named isGreaterThan18 , pass a value , the function must return true if the given age is greater or equals to 18 otherwise false

let isGreaterThan18 = (age) => {
  if (age >= 18) {
    return true;
  } else {
    return false;
  }
};
let years = isGreaterThan18(19);
console.log(years);
