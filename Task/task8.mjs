//"nitan" => "Nitan"

// let capitalFirst = (name) => {
//   return name.charAt(0).toUpperCase() + name.slice(1);
// };
// let output = capitalFirst("nitan");
// console.log(output);

// let capitalFirst = (name) => {
//   let arrayName = name.split("");
//   arrayName[0] = arrayName[0].toUpperCase();

//   let output = arrayName.join("");
//   return output;
// };
// let _capitalFirst = capitalFirst("anish");
// console.log(_capitalFirst);

let capitalFirst = (name) => {
  let arrayName = name.split("");
  let output = arrayName.map((value, index) => {
    if (index === 0) {
      return value.toUpperCase();
    } else {
      return value.toLowerCase();
    }
  });
  let final = output.join("");
  return final;
};

let _capitalFirst = capitalFirst("anish");
console.log(_capitalFirst);
