//  make a arrow function named isEven , pass a value, that retrun true if the given number is even else return false

let isEven = (num) => {
  if (num % 2 === 0) {
    return true;
  } else {
    return false;
  }
};
let _isEven = isEven(10);
console.log(_isEven);

//make a arrow function that take a number and return you can enter room only if the enter number is less than 18 else you can not enter

let number = (num) => {
  if (num < 18) {
    return "You can enter room";
  } else {
    return "You cannot enter room";
  }
};
let answer = number(17);
console.log(answer);

//make a arrow function that takes 3 input as number and return average of given number

let average = (num1, num2, num3) => {
  return (num1 + num2 + num3) / 3;
};
let avg = average(20, 40, 80);
console.log(avg);

//make a arrow function that takes one input as number and return "category1" for number range from 1 to10,  return "category2" for number range from 11 to 20, return "category3" for number range form 21 to 30

let num = (num1) => {
  if (num1 <= 10) {
    return ("category1")
  } else if (num1 <= 20 && num1 >= 11) {
    return ("category2")
  } else {
    return ("category3")
  }
}
let number1 = num(20)
console.log(number1);