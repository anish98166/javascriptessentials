//undefined means variable is defined but not initialize
//null means variable is defined and is initialize will null
let a;
console.log(a);
a = 5;
console.log(a);
a = null;
console.log(a);
