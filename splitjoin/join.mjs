//input array => output string
let arr = ['n', 'i', 't', 'a', 'n']
let arr1 = arr.join("*")
let arr2 = arr.join(" ")
let arr3 = arr.join("")
console.log(arr1); //n * i * t * a * n
console.log(arr2); //n i t a n
console.log(arr3); //nitan
