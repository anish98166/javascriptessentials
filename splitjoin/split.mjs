//split convert string to array

let str = 'abcdecf'
console.log(str.split("c")); //['ab', 'de', 'f']

let str1 = 'my name is anish'
console.log(str1.split(" ")); //[ 'my', 'name', 'is', 'anish' ]
console.log(str1.split(""));
//output
// [
//     'm', 'y', ' ', 'n',
//     'a', 'm', 'e', ' ',
//     'i', 's', ' ', 'a',
//     'n', 'i', 's', 'h'
// ]

//"my name is" => "myn namen isn"
let output = 'my name is'
let output1 = output.split(" ")
console.log(output1);
let something = output1.map((value, index) => {
    return (`${value}n`)
})
// console.log(something);
let something1 = something.join(" ")
console.log(something1);