// Sum Function
let sum = (num1, num2) => {
  console.log(num1 + num2);
};
sum(1, 2);

// Multiply Function
let multiply = (num1, num2) => {
  console.log(num1 * num2);
};
multiply(4, 4);

// Subtract Function
let sub = (num1, num2) => {
  console.log(num1 - num2);
};
sub(10, 2);

// Divide Function
let divide = (num1, num2) => {
  console.log(num1 * num2);
};
divide(7, 2);

// Average Function
let avg = (num1, num2, num3) => {
  // const total = num1 + num2 + num3;
  // const average = total / 3;
  // return average;
  return (num1 + num2 + num3) / 3; //It is better approach
};
const makeAvg = avg(20, 30, 50);
console.log(makeAvg);

// Add Function
let add = (num1, num2) => {
  // let num3 = num1 + num2;
  // return num3;
  return num1 + num2; //It is better approach
};
let sums = add(3, 2);
console.log(sums);
