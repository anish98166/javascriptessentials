console.log(1 + 1); //2
console.log("a" * "b"); //NaN: not a number
console.log("a" + "b"); //ab

//how to check NaN
//we cannot check NaN from this way

let a = NaN;
console.log(a === NaN); //false
//To check NaN
console.log(isNaN(a)); //true
