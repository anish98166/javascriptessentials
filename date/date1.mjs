//it gives the current date and time
console.log(new Date());
//new Date() gives date in iso format : yyyy-mm-ddThh:mm:ss
console.log(new Date().toDateString());
console.log(new Date().toLocaleString());
console.log(new Date().toLocaleTimeString());
console.log(new Date().toLocaleDateString());
