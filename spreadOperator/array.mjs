//Spread Operator are wrapper opener.

let arr = ["a", "b", "c"];
let arr1 = [1, 2, 3];
//["a","b","c",1,2,3]
let arr2 = [3, 4, ...arr, 5, ...arr1, 7];

console.log(arr2);
console.log([...arr, ...arr1]);
