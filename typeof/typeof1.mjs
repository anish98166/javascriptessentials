//Primitive type

let a = "anish";
console.log(typeof a);
console.log(typeof 1);
console.log(typeof false);

//for all non primitive types is always object

console.log(typeof [1, 2]);
console.log({ name: "anish" });
