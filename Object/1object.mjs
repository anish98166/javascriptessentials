let info = {
    //array is a collection of values whereas object is a collection of key value pairs. Key value pairs are called properties.
    //key : value
    name: "anish",
    age: 27,
    isMarried: false,
}

//get value
console.log(info);
console.log(info.name);
console.log(info.age);
console.log(info.isMarried);
//change value
info.age = 30;
info.isMarried = true
console.log(info);
//delete value
delete info.isMarried
console.log(info);