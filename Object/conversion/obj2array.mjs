let info = {
    name: 'anish',
    age: 30,
    isMarried: false,
}

let keysArray = Object.keys(info)//[ 'name', 'age', 'isMarried' ]
console.log(keysArray);

let valueArray = Object.values(info)//[ 'anish', 30, false ]
console.log(valueArray);

let propertyArray = Object.entries(info)
console.log(propertyArray);//[ [ 'name', 'anish' ], [ 'age', 30 ], [ 'isMarried', false ] ]