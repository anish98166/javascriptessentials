{
  //parent block
  let a = 1;
  {
    //child block
    console.log(a);
  }
  console.log(a);
}

//when a variable is called first it is searched in its own block if variable is not find in that block then it will search for its parent block
