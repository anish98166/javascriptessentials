{
  console.log(a); //gives error
  let a = 1;
  console.log(a); // gives output 1
}
console.log(a); //gives error

//variable will be known within its block only from the line where it is defined
//a will be known from line 3 to 5
