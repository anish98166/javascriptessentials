//Lexical Scope

{
  //parent block
  let a = 10;
  console.log(a); //10
  {
    //child block
    a = 12;
    console.log(a); //12
  }
  console.log(a); //12
}
