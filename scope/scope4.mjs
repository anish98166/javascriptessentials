{
  //parent block
  let a = 1;
  let b = 10;
  {
    //child block
    let a = 2;
    console.log(a); //2
    console.log(b); //10
  }
  console.log(a); //1
}
