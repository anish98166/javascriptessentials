{
  //parent block
  let a = 1;
  {
    //child block
    let a = 2;
  }
}

//a variable can't be redefined twice in same block but we can define same variable in different block.
